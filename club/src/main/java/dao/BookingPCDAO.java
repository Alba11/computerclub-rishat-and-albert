package dao;

import dto.BookingPC;
import dto.Computer;

import java.sql.Timestamp;
import java.util.List;

public interface BookingPCDAO {
    boolean saveBooking(BookingPC bookingPC);
    List<Computer> getNotBusyComputerInTime(Timestamp from, Timestamp to);
    boolean deleteBookingById(int id);
    List<BookingPC> getBookingsByUserId(int user_id);
}
