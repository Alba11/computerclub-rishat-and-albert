package dao;

import dto.BookingPC;
import dto.Computer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class BookingPCDAOMySql implements BookingPCDAO{
    private final MySqlConnection mySqlConnection = new MySqlConnection();

    @Override
    public boolean saveBooking(BookingPC bookingPC) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "INSERT INTO schema_user.booking_pc (user_id, computer_id, price, time_from, time_to) values(?, ?, ?, ?, ?)";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, bookingPC.getUser_id());
                statement.setInt(2, bookingPC.getComputer_id());
                statement.setDouble(3, bookingPC.getPrice());
                statement.setTimestamp(4, bookingPC.getTimeFrom());
                statement.setTimestamp(5, bookingPC.getTimeTo());

                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Computer> getNotBusyComputerInTime(Timestamp from, Timestamp to) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.computer as c LEFT JOIN schema_user.booking_pc as b ON c.id = b.computer_id WHERE (b.time_from IS NULL OR b.time_from > ? OR b.time_to < ?)";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                LinkedList<Computer> computers = new LinkedList<>();
                statement.setTimestamp(1, to);
                statement.setTimestamp(2, from);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Computer computer = new Computer();
                    computer.setId(resultSet.getInt("id"));
                    computer.setPriceForHour(resultSet.getDouble("priceForHour"));
                    computer.setRating(resultSet.getInt("rating"));
                    computers.add(computer);
                }
                return computers;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteBookingById(int id) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "DELETE FROM schema_user.booking_pc WHERE id = ?";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, id);
                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<BookingPC> getBookingsByUserId(int user_id) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.booking_pc WHERE user_id = ?";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                LinkedList<BookingPC> bookingPCs = new LinkedList<>();
                statement.setInt(1, user_id);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    BookingPC bookingPC = new BookingPC();
                    bookingPC.setId(resultSet.getInt("id"));
                    bookingPC.setUser_id(resultSet.getInt("user_id"));
                    bookingPC.setComputer_id(resultSet.getInt("computer_id"));
                    bookingPC.setPrice(resultSet.getDouble("price"));
                    bookingPC.setTimeFrom(resultSet.getTimestamp("time_from"));
                    bookingPC.setTimeTo(resultSet.getTimestamp("time_to"));

                    bookingPCs.add(bookingPC);
                }
                return bookingPCs;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}
