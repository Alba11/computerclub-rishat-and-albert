package dao;

import dto.Computer;

public interface ComputerDAO {
    Computer getComputerById(int id);
    boolean saveComputer(Computer computer);
}
