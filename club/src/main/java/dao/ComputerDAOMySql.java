package dao;

import dto.Computer;
import dto.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ComputerDAOMySql implements ComputerDAO{
    private final MySqlConnection mySqlConnection = new MySqlConnection();

    @Override
    public Computer getComputerById(int id) {
        try(Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.computer WHERE user_id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, id);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    Computer computer = new Computer();
                    computer.setId(resultSet.getInt("id"));
                    computer.setPriceForHour(resultSet.getDouble("priceForHour"));
                    computer.setRating(resultSet.getInt("rating"));
                    return computer;
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean saveComputer(Computer computer) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "INSERT INTO schema_user.computer (priceForHour, rating) values(?, ?)";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setDouble(1, computer.getPriceForHour());
                statement.setInt(2, computer.getRating());

                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }
}
