package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnection {
    public Connection getNewConnection() {
        try {
            String url = "jdbc:mysql://localhost:3306/schema_user?useUnicode=true&serverTimezone=Europe/Moscow";
            String user = "root";
            String password = "12344321";
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException | ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
