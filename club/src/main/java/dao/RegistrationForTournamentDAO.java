package dao;

import dto.BookingTournament;
import dto.Tournament;

import java.util.List;

public interface RegistrationForTournamentDAO {
    boolean saveNewUser(BookingTournament bookingTournament);
    boolean deleteUser(int tournament_id, int user_id);
    List<Tournament> getTournamentsByUserId(int user_id);
}
