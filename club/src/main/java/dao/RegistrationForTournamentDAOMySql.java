package dao;

import dto.BookingTournament;
import dto.Computer;
import dto.Tournament;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class RegistrationForTournamentDAOMySql implements RegistrationForTournamentDAO{
    private final MySqlConnection mySqlConnection = new MySqlConnection();

    @Override
    public boolean saveNewUser(BookingTournament bookingTournament) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "INSERT INTO schema_user.registration_for_tournament (tournament_id, user_id) values(?, ?)";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, bookingTournament.getTournament_id());
                statement.setInt(2, bookingTournament.getUser_id());

                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteUser(int tournament_id, int user_id) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "DELETE FROM schema_user.registration_for_tournament WHERE tournament_id = ? AND user_id = ?";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, tournament_id);
                statement.setInt(2, user_id);
                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Tournament> getTournamentsByUserId(int user_id) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.tournament as t JOIN schema_user.registration_for_tournament as rft ON t.id = rft.tournament_id WHERE rft.user_id = ?";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                LinkedList<Tournament> tournaments = new LinkedList<>();
                statement.setInt(1, user_id);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Tournament tournament = new Tournament();
                    tournament.setId(resultSet.getInt("id"));
                    tournament.setUser_id(resultSet.getInt("user_id"));
                    tournament.setName(resultSet.getString("name"));
                    tournament.setTimeFrom(resultSet.getTimestamp("time_from"));
                    tournament.setTimeTo(resultSet.getTimestamp("time_to"));
                    tournaments.add(tournament);
                }
                return tournaments;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}
