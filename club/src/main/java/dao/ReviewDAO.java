package dao;

import dto.Review;

import java.util.List;

public interface ReviewDAO {
    boolean saveReview(Review review);
    List<Review> getProjectReviewByProjectId();
}
