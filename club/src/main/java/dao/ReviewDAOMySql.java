package dao;

import dto.Review;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ReviewDAOMySql implements ReviewDAO{
    private final MySqlConnection connection = new MySqlConnection();

    @Override
    public boolean saveReview(Review review) {
        try (Connection conn = connection.getNewConnection()) {
            String sql = "INSERT INTO schema_user.review_club (user_id, title, `date`, rating) values (?, ?, ?, ?)";
            System.out.println("Connection is done");
            try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                preparedStatement.setInt(1, review.getUser_id());
                preparedStatement.setString(2, review.getTitle());
                preparedStatement.setDate(3, review.getDate());
                preparedStatement.setDouble(4, review.getRating());

                preparedStatement.executeUpdate();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Review> getProjectReviewByProjectId() {
        try (Connection conn = connection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.review_club as r JOIN schema_user.user as u ON r.user_id = u.user_id";
            try(PreparedStatement statement = conn.prepareStatement(sql)){
                LinkedList<Review> reviews = new LinkedList<>();
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Review review = new Review();
                    review.setId(resultSet.getInt("id"));
                    review.setUser_id(resultSet.getInt("user_id"));
                    review.setUsername(resultSet.getString("username"));
                    review.setTitle(resultSet.getString("title"));
                    review.setDate(resultSet.getDate("date"));
                    review.setRating(resultSet.getDouble("rating"));
                    reviews.add(review);
                }
                return reviews;
            }
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
