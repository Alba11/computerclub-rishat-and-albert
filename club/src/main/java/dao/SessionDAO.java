package dao;

import dto.Session;
import dto.User;

public interface SessionDAO {
    Session getSessionByUserId(int user_id);
    Session getSessionById(String session_id);
    boolean saveSessionDAO(Session session);
    boolean removeSessionById(String session_id);
    boolean removeSessionByUserId(int user_id);
}
