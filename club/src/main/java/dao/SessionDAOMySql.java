package dao;

import dto.Session;
import dto.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SessionDAOMySql implements SessionDAO {
    MySqlConnection mySqlConnection = new MySqlConnection();

    @Override
    public Session getSessionByUserId(int user_id) {
        try(Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.user_session WHERE user_id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, user_id);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    Session session = new Session();
                    session.setSession_id(resultSet.getString("session_id"));
                    session.setUser_id(resultSet.getInt("user_id"));
                    return session;
                }
            }
        }catch (SQLException e){
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public Session getSessionById(String session_id) {
        try(Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.user_session WHERE session_id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setString(1, session_id);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    System.out.println("yes");
                    Session session = new Session();
                    session.setSession_id(resultSet.getString("session_id"));
                    session.setUser_id(resultSet.getInt("user_id"));
                    return session;
                }
            }
        }catch (SQLException e){
            e.printStackTrace();

        }
        return null;
    }


    @Override
    public boolean saveSessionDAO(Session session) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "INSERT INTO schema_user.user_session (session_id, user_id) values(?, ?)";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setString(1, session.getSession_id());
                statement.setInt(2, session.getUser_id());

                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean removeSessionById(String session_id) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "DELTE FROM schema_user.user_session WHERE session_id = ?";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setString(1, session_id);

                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean removeSessionByUserId(int user_id) {
        try (Connection conn = mySqlConnection.getNewConnection()){
            String sql = "DELETE FROM schema_user.user_session WHERE user_id = ?";
            try(PreparedStatement statement = conn.prepareStatement(sql)){
                statement.setInt(1, user_id);
                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }
}
