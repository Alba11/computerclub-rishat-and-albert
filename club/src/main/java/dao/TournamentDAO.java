package dao;

import dto.Tournament;

import java.util.List;

public interface TournamentDAO {
    boolean saveTournament(Tournament tournament);
    boolean deleteTournamentById(int id);
    List<Tournament> getAllTournaments();
}
