package dao;

import dto.Computer;
import dto.Tournament;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class TournamentDAOMySql implements TournamentDAO {
    private final MySqlConnection mySqlConnection = new MySqlConnection();

    @Override
    public boolean saveTournament(Tournament tournament) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "INSERT INTO schema_user.tournament (user_id, time_from, time_to, `name`) values(?, ?, ?, ?)";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, tournament.getUser_id());
                statement.setTimestamp(2, tournament.getTimeFrom());
                statement.setTimestamp(3, tournament.getTimeTo());
                statement.setString(4, tournament.getName());

                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteTournamentById(int id) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "DELETE FROM schema_user.tournament WHERE id = ?";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, id);
                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Tournament> getAllTournaments() {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.tournament";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                LinkedList<Tournament> tournaments = new LinkedList<>();
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    Tournament tournament = new Tournament();
                    tournament.setId(resultSet.getInt("id"));
                    tournament.setUser_id(resultSet.getInt("user_id"));
                    tournament.setTimeFrom(resultSet.getTimestamp("time_from"));
                    tournament.setTimeTo(resultSet.getTimestamp("time_to"));
                    tournament.setName(resultSet.getString("name"));
                    System.out.println("+1 tournament id:" + tournament.getId());
                    tournaments.add(tournament);
                }
                return tournaments;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}
