package dao;

import dto.User;

public interface UserDAO {
    User getUserById(int id);
    User getUserByUsername(String username);
    boolean saveUserDAO(User admin);
    User findUser(String username);
}
