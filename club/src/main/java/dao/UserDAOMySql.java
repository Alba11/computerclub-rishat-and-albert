package dao;

import dto.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAOMySql implements UserDAO {
    private final MySqlConnection mySqlConnection = new MySqlConnection();

    @Override
    public User getUserById(int id) {
        try(Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.user WHERE user_id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, id);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    User user = new User();
                    user.setUser_id(resultSet.getInt("user_id"));
                    user.setUserName(resultSet.getString("username"));
                    return user;
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getUserByUsername(String username) {
        try(Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.user WHERE username = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setString(1, username);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    User user = new User();
                    user.setUser_id(resultSet.getInt("user_id"));
                    user.setUserName(resultSet.getString("username"));
                    return user;
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean saveUserDAO(User user) {
        try (Connection connection = mySqlConnection.getNewConnection()){
            String sql = "INSERT INTO schema_user.user (username, password) values(?, ?)";
            try(PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setString(1, user.getUserName());
                statement.setString(2, user.getPassword());

                statement.executeUpdate();
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public User findUser(String username) {
        try(Connection connection = mySqlConnection.getNewConnection()){
            String sql = "SELECT * FROM schema_user.user WHERE username = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setString(1, username);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    User user = new User();
                    user.setUser_id(resultSet.getInt("user_id"));
                    user.setUserName(resultSet.getString("username"));
                    user.setPassword(resultSet.getString("password"));
                    return user;
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}
