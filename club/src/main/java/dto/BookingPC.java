package dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class BookingPC {
    private int id;
    private int user_id;
    private int computer_id;
    private double price;
    private Timestamp timeFrom;
    private Timestamp timeTo;
}
