package dto;

import lombok.Data;

@Data
public class BookingTournament {
    private int id;
    private int tournament_id;
    private int user_id;
    private int pc_id;
}
