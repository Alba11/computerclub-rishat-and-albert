package dto;

import lombok.Data;

@Data
public class Computer {
    private int id;
    private double priceForHour;
    private int rating;
}
