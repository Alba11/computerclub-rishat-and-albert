package dto;

import lombok.Data;

import java.sql.Date;

@Data
public class Review {
    private int id;
    private int user_id;
    private String username;
    private double rating;
    private String title;
    private Date date;
}
