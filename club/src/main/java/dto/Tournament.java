package dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class Tournament {
    private int id;
    private int user_id;
    private String name;
    private Timestamp timeFrom;
    private Timestamp timeTo;
}
