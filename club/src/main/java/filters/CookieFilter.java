package filters;

import dao.SessionDAO;
import dao.SessionDAOMySql;
import dto.Session;
import dto.User;
import service.SessionService;
import service.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/*"})
public class CookieFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        HttpSession httpSession = request.getSession();
        servletRequest.setCharacterEncoding("UTF-8");

        if (httpSession.getAttribute("user") == null) {
            Cookie[] cookies = request.getCookies();
            String sessionID = null;
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("session_id")) {
                        sessionID = cookie.getValue();
                        break;
                    }
                }
             }
            SessionService sessionService = new SessionService();
            UserService userService = new UserService();
            Session session = sessionService.getSessionById(sessionID);
            if (session != null) {
                User user = userService.getUserById(session.getUser_id());
                request.getSession().setAttribute("user", user);
                request.getSession().setAttribute("username", user.getUserName());
                request.getSession().setAttribute("user_id", user.getUser_id());
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
