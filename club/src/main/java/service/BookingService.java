package service;

import dao.BookingPCDAO;
import dao.BookingPCDAOMySql;
import dto.BookingPC;
import dto.Computer;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BookingService {
    private final BookingPCDAO bookingPCDAO = new BookingPCDAOMySql();
    private final String DATE_FORMAT = "yyyy-mm-dd HH:mm";
    private final int HOUR = 3600*1000;
    private final SimpleDateFormat FORMAT = new SimpleDateFormat(DATE_FORMAT);

    public boolean addBooking(int user_id, int computer_id, String from, int hours, double price) throws ParseException {
        BookingPC bookingPC = new BookingPC();
        bookingPC.setUser_id(user_id);
        bookingPC.setComputer_id(computer_id);
        bookingPC.setPrice(price * hours);

        Date dateFrom = FORMAT.parse(from);
        Date dateTo = new Date(dateFrom.getTime() + hours*HOUR);

        bookingPC.setTimeFrom(new Timestamp(dateFrom.getTime()));
        bookingPC.setTimeTo(new Timestamp(dateTo.getTime()));

        return bookingPCDAO.saveBooking(bookingPC);
    }

    public List<Computer> getFreePC(String from, int hours) throws ParseException {
        Date dateFrom = FORMAT.parse(from);
        Date dateTo = new Date(dateFrom.getTime() + hours*HOUR);
        return bookingPCDAO.getNotBusyComputerInTime(new Timestamp(dateFrom.getTime()), new Timestamp(dateTo.getTime()));
    }

    public boolean deleteBooking(int booking_id){
        return bookingPCDAO.deleteBookingById(booking_id);
    }

    public List<BookingPC> getBookingsByUserId(int user_id){
        return bookingPCDAO.getBookingsByUserId(user_id);
    }
}
