package service;

import dao.ComputerDAO;
import dao.ComputerDAOMySql;
import dto.Computer;

public class ComputerService {
    private final ComputerDAO computerDAO = new ComputerDAOMySql();

    public Computer getComputerById(int id) {
        return computerDAO.getComputerById(id);
    }
}
