package service;

import dao.ReviewDAO;
import dao.ReviewDAOMySql;
import dto.Review;

import java.util.Date;
import java.util.List;

public class ReviewService {
    private final ReviewDAO reviewDAO = new ReviewDAOMySql();
    public boolean saveReview(int user_id, String title, Date date, double rating){
        Review review = new Review();
        review.setUser_id(user_id);
        review.setTitle(title);
        review.setDate(new java.sql.Date(date.getTime()));
        review.setRating(rating);

        return reviewDAO.saveReview(review);
    }

    public List<Review> getReviewsByCompletedProjectId(){ return reviewDAO.getProjectReviewByProjectId(); }

}
