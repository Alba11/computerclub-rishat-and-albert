package service;

import dao.SessionDAO;
import dao.SessionDAOMySql;
import dto.Session;
import dto.User;

import java.util.ArrayList;
import java.util.List;


public class SessionService {
    private final SessionDAO sessionDAO = new SessionDAOMySql();

    public Session getSessionByUserId(int user_id){
        return sessionDAO.getSessionByUserId(user_id);
    }

    public Session getSessionById(String session_id){
        return sessionDAO.getSessionById(session_id);
    }

    public List<String> enrollSession(String session_id, int user_id) {
        List<String> errors = new ArrayList<>();

        if (session_id == null) errors.add("session is not exist");
        if (user_id < 0) errors.add("session is not have user");
        if (!errors.isEmpty()) return errors;

        Session session = new Session();
        session.setUser_id(user_id);
        session.setSession_id(session_id);

        sessionDAO.saveSessionDAO(session);
        return null;
    }

    public boolean removeSessionByUserId(int user_id){
        return sessionDAO.removeSessionByUserId(user_id);
    }
}
