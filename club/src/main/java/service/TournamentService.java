package service;

import dao.RegistrationForTournamentDAO;
import dao.RegistrationForTournamentDAOMySql;
import dao.TournamentDAO;
import dao.TournamentDAOMySql;
import dto.BookingTournament;
import dto.Tournament;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TournamentService {
    private final TournamentDAO tournamentDAO = new TournamentDAOMySql();
    private final RegistrationForTournamentDAO registration = new RegistrationForTournamentDAOMySql();

    private final String DATE_FORMAT = "yyyy-MM-dd";

    public boolean createTournament(int user_id, String name, String stringFrom, String stringTo) throws ParseException {
        if (user_id == 0 || name == null || stringFrom == null || stringTo == null) {
            return false;
        }else{
            SimpleDateFormat format=new SimpleDateFormat(DATE_FORMAT);

            Date dateFrom = format.parse(stringFrom);
            Date dateTo = format.parse(stringTo);

            Tournament tournament = new Tournament();
            tournament.setUser_id(user_id);
            tournament.setName(name);
            tournament.setTimeFrom(new Timestamp(dateFrom.getTime()));
            tournament.setTimeTo(new Timestamp(dateTo.getTime()));
            return tournamentDAO.saveTournament(tournament);
        }
    }

    public boolean addUserToTournament(int user_id, int tournament_id){
        if(user_id == 0 || tournament_id == 0){
            return false;
        }else{
            BookingTournament bookingTournament = new BookingTournament();
            bookingTournament.setUser_id(user_id);
            bookingTournament.setTournament_id(tournament_id);
            return registration.saveNewUser(bookingTournament);
        }
    }

    public List<Tournament> getAllTournaments(){
        return tournamentDAO.getAllTournaments();
    }

    public boolean deleteUserFromTournament(int user_id, int tournament_id){
        return registration.deleteUser(tournament_id, user_id);
    }

    public List<Tournament> getTournamentsByUserId(int user_id){
        return registration.getTournamentsByUserId(user_id);
    }
}
