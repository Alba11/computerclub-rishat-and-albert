package service;

import dao.UserDAO;
import dao.UserDAOMySql;
import dto.User;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class UserService {
    private static final String SALT = "AlbA";
    private UserDAO userDAO = new UserDAOMySql();

    public User findUser(String username, String password){
        if(username != null && password != null){
            User userData = userDAO.findUser(username);
            if (userData == null) return null;
            String hashPassword = generateSecurePassword(password);
            if(hashPassword.equals(userData.getPassword())){
                return userData;
            }
            return null;
        }
        return null;
    }

    public boolean userIsExist(String username){
        User user = userDAO.findUser(username);
        return user != null;
    }

    public User getUserById(int id){
        if(id >= 0){
            User userData = userDAO.getUserById(id);
            if (userData == null) return null;
            return userData;
        }
        return null;
    }

    public List<String> enrollUser(String username, String password) {
        List<String> errors = new ArrayList<>();

        if (username == null) errors.add("username is empty");
        if (password == null) errors.add("password is empty");
        User user = new User();
        user.setUserName(username);
        user.setPassword(generateSecurePassword(password));

        userDAO.saveUserDAO(user);
        return null;
    }

    private String generateSecurePassword(String password) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-512");
            messageDigest.update(SALT.getBytes());
            byte[] newValue = messageDigest.digest(password.getBytes(StandardCharsets.UTF_8));
            return java.util.Base64.getEncoder().encodeToString(newValue);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return password;
    }

}
