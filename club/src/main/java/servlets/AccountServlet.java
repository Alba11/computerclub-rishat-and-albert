package servlets;

import dto.BookingPC;
import dto.Tournament;
import service.BookingService;
import service.TournamentService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/account")
public class AccountServlet extends HttpServlet {
    private final TournamentService tournamentService = new TournamentService();
    private final BookingService bookingService = new BookingService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int user_id = (Integer)req.getSession().getAttribute("user_id");

        List<Tournament> userTournaments = tournamentService.getTournamentsByUserId(user_id);
        List<BookingPC> bookings = bookingService.getBookingsByUserId(user_id);
        req.setAttribute("userTournaments", userTournaments);
        req.setAttribute("userBookings", bookings);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/account.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
