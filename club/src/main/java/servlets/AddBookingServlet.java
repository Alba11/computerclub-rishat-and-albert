package servlets;

import lombok.SneakyThrows;
import service.BookingService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addBooking")
public class AddBookingServlet extends HttpServlet {
    private final BookingService bookingService = new BookingService();
    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int user_id = (Integer) req.getSession().getAttribute("user_id");
        int computer_id = Integer.parseInt(req.getParameter("computer_id"));
        String date = req.getParameter("date");
        int hours = Integer.parseInt(req.getParameter("hours"));
        double price = Double.parseDouble(req.getParameter("price"));

        bookingService.addBooking(user_id, computer_id, date, hours, price);

        resp.sendRedirect("/pcclub/account");
    }
}
