package servlets;

import service.BookingService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/cancelBooking")
public class CancelBookingServlet extends HttpServlet {
    private final BookingService bookingService = new BookingService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int booking_id = Integer.parseInt(req.getParameter("booking_id"));

        bookingService.deleteBooking(booking_id);

        resp.sendRedirect("/pcclub/account");
    }
}
