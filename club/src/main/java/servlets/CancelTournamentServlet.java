package servlets;

import service.TournamentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/cancelTournament")
public class CancelTournamentServlet extends HttpServlet {
    private final TournamentService tournamentService = new TournamentService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int user_id = (Integer) req.getSession().getAttribute("user_id");
        int tournament_id = Integer.parseInt(req.getParameter("tournament_id"));

        tournamentService.deleteUserFromTournament(user_id, tournament_id);

        resp.sendRedirect("/pcclub/account");
    }
}
