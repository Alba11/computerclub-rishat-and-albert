package servlets;

import dto.Computer;
import liquibase.pro.packaged.C;
import lombok.SneakyThrows;
import service.BookingService;
import service.ComputerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@WebServlet("/computerBooking")
public class ComputerBookingServlet extends HttpServlet {
    private final BookingService bookingService = new BookingService();
    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String dateString = req.getParameter("date");
        String timeString = req.getParameter("time");
        int hours = Integer.parseInt(req.getParameter("hours"));
        System.out.println(hours);
        LocalDate datePart = LocalDate.parse(dateString);
        LocalTime timePart = LocalTime.parse(timeString);
        LocalDateTime dt = LocalDateTime.of(datePart, timePart);
        System.out.println(dt);
        List<Computer> computers = bookingService.getFreePC(dateString + " " + timeString, hours);
        req.setAttribute("computers", computers);
        req.setAttribute("date", dateString + " " + timeString);
        req.setAttribute("hours", req.getParameter("hours"));
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/computerBooking.jsp");
        dispatcher.forward(req, resp);
    }
}
