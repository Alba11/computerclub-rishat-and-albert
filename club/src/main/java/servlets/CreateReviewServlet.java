package servlets;

import service.ReviewService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/createReview")
public class CreateReviewServlet extends HttpServlet {
    private final ReviewService reviewService = new ReviewService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int user_id = (Integer) req.getSession().getAttribute("user_id");
        double rating = Double.parseDouble(req.getParameter("rating"));
        String title = req.getParameter("title");

        reviewService.saveReview(user_id, title, new Date(), rating);

        resp.sendRedirect("/pcclub/reviews");
    }

}
