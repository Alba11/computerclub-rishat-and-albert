package servlets;

import dao.TournamentDAO;
import dao.TournamentDAOMySql;
import lombok.SneakyThrows;
import service.TournamentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;

@WebServlet("/createTournament")
public class CreateTournamentServlet extends HttpServlet {
    private final TournamentService tournamentService = new TournamentService();

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         int user_id = (Integer)req.getSession().getAttribute("user_id");
         String tournament_name = req.getParameter("tournament_name");
         String timeFrom = req.getParameter("dateFrom");
         String timeTo = req.getParameter("dateTo");

         tournamentService.createTournament(user_id, tournament_name, timeFrom, timeTo);

        resp.sendRedirect("/pcclub/tournaments");
    }
}
