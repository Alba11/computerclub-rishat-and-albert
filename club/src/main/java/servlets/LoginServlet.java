package servlets;


import dao.UserDAO;
import dao.UserDAOMySql;
import dto.Session;
import dto.User;
import service.SessionService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String username = req.getParameter("username");
        String password = req.getParameter("password");

        UserService userService = new UserService();
        SessionService sessionService = new SessionService();
        User user = userService.findUser(username, password);

        if (user == null) {
            resp.sendRedirect("/pcclub/login");
        }else{
            System.out.println("userFound");
            req.getSession().setAttribute("user", user);
            req.getSession().setAttribute("username", user.getUserName());
            req.getSession().setAttribute("user_id", user.getUser_id());

            if(req.getParameter("rememberMe") != null) {
                sessionService.enrollSession(req.getSession().getId(), user.getUser_id());
                Cookie cookie = new Cookie("session_id", req.getSession().getId());
                cookie.setDomain(req.getServerName());
                cookie.setPath(req.getContextPath());
                cookie.setMaxAge(60 * 60 * 24 * 30);
                resp.addCookie(cookie);
            }
            resp.sendRedirect("/pcclub/account");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/signIn.jsp");
        dispatcher.forward(req, resp);
    }
}
