package servlets;

import service.SessionService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    SessionService sessionService = new SessionService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
//        String username = (String) session.getAttribute("user");
//        UserDataBase.deleteSessionForUser(username);
        sessionService.removeSessionByUserId((int)session.getAttribute("user_id"));
        session.removeAttribute("username");
        session.removeAttribute("user");
        session.removeAttribute("password");
        session.removeAttribute("user_id");
        Cookie cookie = new Cookie("session_id", "expired");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
        req.getSession().invalidate();
        resp.sendRedirect("/pcclub/index");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
