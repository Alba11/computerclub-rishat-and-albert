package servlets;

import dto.Computer;
import lombok.SneakyThrows;
import service.BookingService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/reservation")
public class ReservationServlet extends HttpServlet {
    private final BookingService bookingService = new BookingService();

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int user_id = Integer.parseInt(req.getParameter("user_id"));
        String dateFrom = req.getParameter("dateFrom");
        String timeFrom = req.getParameter("timeFrom");
        int hours = Integer.parseInt(req.getParameter("hours"));
        List<Computer> computers = bookingService.getFreePC(dateFrom, hours);
        req.setAttribute("computers", computers);
        req.getServletContext().getRequestDispatcher("/reservation.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/booking.jsp");
        dispatcher.forward(req, resp);
    }
}
