package servlets;

import lombok.SneakyThrows;
import service.BookingService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/saveBooking")
public class SaveBookingServlet extends HttpServlet {
    private final BookingService bookingService = new BookingService();
    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int user_id = Integer.parseInt(req.getParameter("user_id"));
        int computer_id = Integer.parseInt(req.getParameter("computer_id"));
        String dateFrom = req.getParameter("dateFrom");
        int hours = Integer.parseInt(req.getParameter("hours"));

//        bookingService.addBooking(user_id, computer_id, dateFrom, hours, );
        req.getServletContext().getRequestDispatcher("/pcclub/account");
    }
}
