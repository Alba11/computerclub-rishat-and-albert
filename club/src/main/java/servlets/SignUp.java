package servlets;

import dto.Session;
import dto.User;
import service.SessionService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signUp")
public class SignUp extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        UserService userService = new UserService();
        SessionService sessionService = new SessionService();

        User user = new User();
        user.setUserName(username);
        user.setPassword(password);
        userService.enrollUser(username, password);
        user = userService.findUser(username, password);
        req.getSession().setAttribute("user", user);
        req.getSession().setAttribute("user_id", user.getUser_id());

        if (req.getParameter("checkbox") != null) {
            sessionService.enrollSession(req.getSession().getId(), user.getUser_id());
            Cookie cookie = new Cookie("session_id", req.getSession().getId());
            cookie.setDomain(req.getServerName());
            cookie.setPath(req.getContextPath());
            cookie.setMaxAge(60 * 60 * 24 * 30);
            resp.addCookie(cookie);
            System.out.println(2);
        }
        resp.sendRedirect(req.getContextPath() + "/account");
        System.out.println(3);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/signUp.jsp");
        dispatcher.forward(req, resp);
    }
}
