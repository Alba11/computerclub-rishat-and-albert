package servlets;

import dto.Tournament;
import service.TournamentService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/tournaments")
public class TournamentsServlet extends HttpServlet {
    private final TournamentService tournamentService = new TournamentService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Tournament> tournaments = tournamentService.getAllTournaments();
        req.setAttribute("tournaments", tournaments);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/tournaments.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
