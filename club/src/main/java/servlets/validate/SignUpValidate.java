package servlets.validate;

import dto.User;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signUpValidate")
public class SignUpValidate extends HttpServlet {
    UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String confirmPassword = req.getParameter("confirmPassword");

        resp.setCharacterEncoding("UTF-8");

        if (username.equals("") || username.length() >= 32) {
            resp.getWriter().write("emptyUsername ");
        }
        if (password.equals("") || password.length() < 4) {
            resp.getWriter().write("emptyPassword ");
        }
        if (!password.equals(confirmPassword)) {
            resp.getWriter().write("errorConfirmPassword ");
        }
        if (userService.userIsExist(username)) {
            resp.getWriter().write("usernameIsExist ");
        }
    }
}
