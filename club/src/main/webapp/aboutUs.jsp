<%--
  Created by IntelliJ IDEA.
  User: rishatlatypov
  Date: 28.10.2020
  Time: 10:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>About Us</title>
    <link rel="icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="ngb.one">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,400i|Poppins:300,400,500,600" rel="stylesheet">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link id="theme" rel="stylesheet" href="css/theme4.css">
</head>
<body id="page-top">
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">PC Club</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="address">Address</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="booking">Booking</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="tournaments">Tournaments</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="aboutUs">About us</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="reviews">Reviews</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="account">Account</a></li>
            </ul>
        </div>
    </div>
</nav>

<section class="about-section text-center" id="about">
    <form method="post" action="aboutUs">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h2 class="text-white mb-4">About Us</h2>
                    <p class="text-white-50">
                        Игровая индустрия развивается с каждым годом все быстрее и быстрее, она становится трендом, за
                        ней следит не только молодежь, но и взрослое поколение, которое застало зарождение игр в 90-х,
                        то тогда им было не до них. Компьютерные клубы вновь возрождаются, потому что игровое сообщество
                        растет и оно имеет потребность в живом общении с единомышленниками. К сожалению, большинство
                        подобных клубов ограничиваются "коробками" на столе и "игровыми", по их мнению, девайсами.
                        Зачастую, все это далеко от того, что нужно геймеру и тем более - от комфорта. Мы же предлагаем
                        готовые решения, которые не только сделают любой клуб уникальным, а игроки будут получать не
                        только удовольствие от игры с максимальной производительностью на лучших девайсах, но и
                        эстетическое удовольствие, от нахождения в нём. Вам не нужно разбираться во всех тонкостях,
                        потребностях, технических характеристиках, мы всё это уже сделали за Вас и предлагаем лишь
                        лучшие комплексные решения.
                    </p>
                </div>
            </div>
        </div>

        <section id="info" class="iconblock center dark1">
            <div class="container border">
                <div class="row m-space">
                    <div class="col-sm-6 col-md-3">
                        <h3>26 ПК</h3>
                        <p>с топовой перефирией и&nbsp;максимальной производительностью</p>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <h3>Турниры</h3>
                        <p>Возможность организовывать и участвовать в турнирах</p>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <h3>Игровые девайсы</h3>
                        <p>и удобные регулируемые кресла </p>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <h3>VIP зона</h3>
                        <p>с шумоизоляцией для комфортной игры </p>
                    </div>
                </div>
            </div>
            <div class="style-toggle"></div>
        </section>
    </form>
</section>

<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="resources/js/scripts.js"></script>
<script src="js/main.min.js" class=""></script>

</body>
</html>
