<%--
  Created by IntelliJ IDEA.
  User: albertahmadiev
  Date: 26.09.2020
  Time: 00:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>PC Club</title>
    <link rel="icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>

</head>
<body id="page-top">
<nav class="navbar navbar-expand-lg navbar-light navbar-shrink fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">PC Club</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="address">Address</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="booking">Booking</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="tournaments">Tournaments</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="aboutUs">About us</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="reviews">Reviews</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="account">Account</a></li>
            </ul>
        </div>
    </div>
</nav>
<br>

<header class="masthead4" id="mast4">
    <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto text-center">
            <h2 class="text-white-50 mx-auto mt-2 mb-5">My bookings</h2>
            <table class="table table-dark table-hover table-bordered">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Compute_id</th>
                    <th scope="col">Price</th>
                    <th scope="col">TimeFrom</th>
                    <th scope="col">TimeTo</th>
                    <th scope="col"></th>
                </tr>
                <c:forEach var="booking" items="${userBookings}">
                    <tr>
                        <form action="cancelBooking?&booking_id=${booking.getId()}" method="post">
                            <th scope="row">${booking.getId()}</th>
                            <td>${booking.getComputer_id()}</td>
                            <td>${booking.getPrice()}</td>
                            <td>${booking.getTimeFrom()}</td>
                            <td>${booking.getTimeTo()}</td>
                            <td><button type="submit" class="btn-outline-dark badge-danger">Cancel</button> </td>
                        </form>
                    </tr>
                </c:forEach>
            </table>
            <h2 class="text-white-50 mx-auto mt-2 mb-5">My tournaments</h2>
            <table class="table table-bordered table-hover table-dark">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">TimeFrom</th>
                    <th scope="col">TimeTo</th>
                    <th scope="col"></th>
                </tr>
                <c:forEach var="tournament" items="${userTournaments}">
                    <tr>
                        <form action="cancelTournament?user_id=<%=request.getSession().getAttribute("user_id")%>&tournament_id=${tournament.getId()}" method="post">
                            <th scope="row">${tournament.getId()}</th>
                            <td>${tournament.getName()}</td>
                            <td>${tournament.getTimeFrom()}</td>
                            <td>${tournament.getTimeTo()}</td>
                            <td><button type="submit" class="btn-outline-dark btn-danger">Cancel</button> </td>
                        </form>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</header>

<form action="logout" method="get">
    <button class="btn btn-primary btn-block" type="submit">Logout</button>
</form>

<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="resources/js/scripts.js"></script>

</body>
</html>
