<%--
  Created by IntelliJ IDEA.
  User: rishatlatypov
  Date: 29.10.2020
  Time: 22:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Address</title>
    <link rel="icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="ngb.one">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,400i|Poppins:300,400,500,600" rel="stylesheet">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link id="theme" rel="stylesheet" href="css/theme4.css">
</head>

<body id="page-top">
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">PC Club</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="address">Address</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="booking">Booking</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="tournaments">Tournaments</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="aboutUs">About us</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="reviews">Reviews</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="account">Account</a></li>
            </ul>
        </div>
    </div>
</nav>
<br>

<!-- Contact -->
<header class="masthead_5">
    <div class="container-fluid no-pad no-max ">
        <center>
            <div class="col-md-3 col-sm-6 mh center extra-pad dark parallax-container address-hold" data-overlay="9"
                 style="">
                <div class="holder">
                    <div class="placer">
                        <h2>PC Club</h2>
                        <p>г. Казань, ул. Деревня универсиады, 18</p>
                        <p>8 (800) 555-35-35</p>
                        <a href="tel:+88005553535" class="btn"><span>Позвонить</span></a>
                    </div>
                </div>
            </div>
        </center>

        <div style="">
            <iframe allowfullscreen="true"
                    frameborder="0" height="100%" src="https://yandex.ru/map-widget/v1/-/CCQ~n2AMtB"
                    width="100%"></iframe>
        </div>

    </div>
</header>

<script src="js/main.min.js" class=""></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (m, e, t, r, i, k, a) {
    m[i] = m[i] || function () {
        (m[i].a = m[i].a || []).push(arguments)
    };
    m[i].l = 1 * new Date();
    k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
})(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
ym(53056837, "init", {clickmap: true, trackLinks: true, accurateTrackBounce: true, webvisor: true}); </script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/53056837" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript> <!-- /Yandex.Metrika counter -->

<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="resources/js/scripts.js"></script>

</body>
</html>
