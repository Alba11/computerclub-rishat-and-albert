function chekSignIn(qualifiedName, value){
    const usernameInput = document.getElementById("username");
    const passwordInput = document.getElementById("password")
    const xhr = new XMLHttpRequest();

    xhr.open("GET", "http://localhost:8080/pcclub/loginValidate?username=" + usernameInput.value + "&password=" + passwordInput.value, false);
    xhr.send();

    const result = xhr.responseText;
    const errors = result.split(" ");
    var isVal = true;
    for (let i = 0; i < errors.length; i++){
        if (errors[i].trim() === "emptyUsername"){
            document.getElementById("usernameError").style.display = "block";
            document.getElementById("submit").setAttribute("disabled", value);
            isVal = false;
        }
        if (errors[i].trim() === "emptyPassword"){
            document.getElementById("passwordError").style.display = "block";
            document.getElementById("submit").setAttribute("disabled", value);
            isVal = false;
        }
        if (errors[i].trim() === "userNotFound"){
            document.getElementById("userIsNotExist").style.display = "block";
            document.getElementById("submit").setAttribute("disabled", value);
            isVal = false;
        }
    }
    if (isVal){
        document.getElementById("usernameError").style.display = "none";
        document.getElementById("passwordError").style.display = "none";
        document.getElementById("userIsNotExist").style.display = "none";
        document.getElementById("submit").removeAttribute("disabled");
    }
}

function chekSignUp(qualifiedName, value){
    const usernameInput = document.getElementById("username");
    const passwordInput = document.getElementById("password")
    const confirmPasswordInput = document.getElementById("confirmPassword")
    const xhr = new XMLHttpRequest();

    xhr.open("GET", "http://localhost:8080/pcclub/signUpValidate?username=" + usernameInput.value + "&password=" + passwordInput.value + "&confirmPassword=" + confirmPasswordInput.value, false);
    xhr.send();

    const result = xhr.responseText;
    const errors = result.split(" ");
    var isVal = true;
    for (let i = 0; i < errors.length; i++){
        if (errors[i].trim() === "emptyUsername"){
            document.getElementById("usernameError").style.display = "block";
            document.getElementById("submit").setAttribute("disabled", value);
            isVal = false;
        }
        if (errors[i].trim() === "emptyPassword"){
            document.getElementById("passwordError").style.display = "block";
            document.getElementById("submit").setAttribute("disabled", value);
            isVal = false;
        }
        if (errors[i].trim() === "errorConfirmPassword"){
            document.getElementById("confirmPasswordError").style.display = "block";
            document.getElementById("submit").setAttribute("disabled", value);
            isVal = false;
        }
        if (errors[i].trim() === "usernameIsExist"){
            document.getElementById("userIsExist").style.display = "block";
            document.getElementById("submit").setAttribute("disabled", value);
            isVal = false;
        }
    }
    if (isVal){
        document.getElementById("userIsExist").style.display = "none";
        document.getElementById("confirmPasswordError").style.display = "none";
        document.getElementById("passwordError").style.display = "none";
        document.getElementById("usernameError").style.display = "none";
        document.getElementById("submit").removeAttribute("disabled");
    }
}
