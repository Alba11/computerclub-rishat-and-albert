<%--
  Created by IntelliJ IDEA.
  User: rishatlatypov
  Date: 30.10.2020
  Time: 16:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Reservation</title>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>
    <link href="css/theme4.css" rel="stylesheet"/>
</head>

<body id="page-top">
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">PC Club</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="address">Address</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="booking">Booking</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="tournaments">Tournaments</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="aboutUs">About us</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="reviews">Reviews</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="account">Account</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="web-in fade-in">
    <header id="neutral" class="dark1 center" data-overlay="8" style="overflow: hidden;">
        <video autoplay muted loop
               style="position: absolute; left: 50%;  transform: translateX(-50%); top: 0;  min-width: 100%; min-height: 100%; opacity: 0.2; overflow: hidden;">
            <source src="resources/video/video.mp4" type="video/mp4">
        </video>
        <div class="header-in">
            <div class="caption">
                <a href="booking" class="btn  start-project"><span>Назад</span></a>
                <form method="post" action="reservation">
                    <input type="date" placeholder="дата брони" name="dateFrom">
                    <input type="time" placeholder="время брони" name="timeFrom">
                    <select name="hours" id="#№">
                        <option disabled>Выберите сколько часов вы хотите играть</option>
                        <option value="0">1</option>
                        <option value="1">2</option>
                        <option value="2">4</option>
                    </select>
                    <button type="submit" class="btn  start-project"><span>Подтвердить</span></button>
                </form>
            </div>
        </div>
        <div class="header-toggle"></div>
    </header>
</div>

<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="resources/js/scripts.js"></script>
<script src="js/main.min.js" class=""></script>

</body>
</html>
