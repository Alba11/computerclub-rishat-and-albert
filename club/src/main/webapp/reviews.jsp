<%--
  Created by IntelliJ IDEA.
  User: rishatlatypov
  Date: 30.10.2020
  Time: 17:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Reviews</title>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="css/styles.css" rel="stylesheet"/>
    <link href="css/review.css" rel="stylesheet"/>
</head>

<body>
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">PC Club</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="address">Address</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="booking">Booking</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="tournaments">Tournaments</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="aboutUs">About us</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="reviews">Reviews</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="account">Account</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- Masthead-->
<header class="masthead_2">
    <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto text-center">
            <h1 class="mx-auto my-0 text-uppercase">Welcome to computer club</h1>
            <h2 class="text-white-50 mx-auto mt-2 mb-5">Here you can escape from the gray everyday life and plunge into
                your world of desires and fantasies.</h2>
            <a class="btn btn-primary js-scroll-trigger" href="aboutUs">Get Started</a>
        </div>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="well">
            <div class="list-group">
                <c:forEach var="review" items="#{reviews}">
                    <div class="blockquote-box clearfix">
                        <div class="square pull-left">
                            <img src="" alt="" class=""/>
                        </div>
                        <h4>${review.getId()}: ${review.getUsername()}</h4>
                        <p>${review.getTitle()}</p>
                        <h4>${review.getRating()}</h4>
                        <h6>${review.getDate()}</h6>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<form method="post" action="createReview">
    <div class="form-group row col-md-3 mb-3">
        <label class="col-sm-2 col-form-label" for="title">Your review:</label>
        <input class="form-control" id="title" type="text" name="title" placeholder="review text">
        <label class="col-sm-2 col-form-label" for="rating">Put your rating</label>
        <p><select class="custom-select" id="rating" name="rating" size="5" multiple>
            <option selected value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
            <button class="btn btn-primary js-scroll-trigger" type="submit">Send</button>
        </p>
    </div>
</form>


<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Core theme JS-->
<script src="resources/js/scripts.js"></script>
<script src="js/review.js"></script>
</body>
</html>
