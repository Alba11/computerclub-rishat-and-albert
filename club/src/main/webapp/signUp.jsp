<%--
  Created by IntelliJ IDEA.
  User: albertahmadiev
  Date: 25.09.2020
  Time: 18:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <title>Sign Up</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="signInCss/main.css">
    <link rel="stylesheet" type="text/css" href="signInCss/util.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/error.css">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>
</head>
<body id="page-top">
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#">PC Club</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="address">Address</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#">Booking</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#">Tournaments</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="aboutUs">About us</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="reviews">Reviews</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#">Account</a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form p-l-55 p-r-55 p-t-178" method="post" action="signUp">
					<span class="login100-form-title">
						Sign Up
					</span>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter username">
                    <input class="input100" id="username" type="text" name="username" placeholder="Username"
                           onblur="chekSignUp()">
                    <span class="focus-input100"></span>
                </div>
                <span class="error" id="usernameError">Username incorrect</span>

                <div class="wrap-input100 validate-input" data-validate="Please enter password">
                    <input class="input100" id="password" type="password" name="password" placeholder="Password"
                           onblur="chekSignUp()">
                    <span class="focus-input100"></span>
                </div>
                <span class="error" id="passwordError">Password incorrect</span>
                <br>

                <div class="wrap-input100 validate-input" data-validate="Please enter password">
                    <input class="input100" id="confirmPassword" type="password" name="password"
                           placeholder="Confirm your password" onblur="chekSignUp()">
                    <span class="focus-input100"></span>
                </div>
                <span class="error" id="confirmPasswordError">Password is not equal</span>
                <span class="error" id="userIsExist">User is exist</span>
                <br>

                <div class="text-right p-t-13 p-b-23">
						<span class="txt1">
							<input type="checkbox" name="rememberMe" value="true" id="rememberMe"> <label
                                for="rememberMe">Remember me</label>
						</span>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn" id="submit">
                        Sign Up
                    </button>
                </div>

                <div class="flex-col-c p-t-170 p-b-40">
                    <a href="login" class="txt3">
                        Sign in
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="resources/js/signInJs/main.js"></script>
<script src="js/validation.js"></script>
<!-- Bootstrap core JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="resources/js/scripts.js"></script>

</body>
</html>