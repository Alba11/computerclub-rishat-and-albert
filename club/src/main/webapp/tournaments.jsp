<%--
  Created by IntelliJ IDEA.
  User: rishatlatypov
  Date: 29.10.2020
  Time: 22:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Tournaments</title>
    <link rel="icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>
    <link href="css/tournaments.css" rel="stylesheet"/>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index">PC Club</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="address">Address</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="booking">Booking</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="tournaments">Tournaments</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="aboutUs">About us</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="reviews">Reviews</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="account">Account</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="page-7">
    <a href="#mast3" class="page-7-m content">
        <h3>
            Организовать свой турнир
        </h3>
    </a>
    <a href="#mast4" class="page-7-xm content">
        <h3>
            Войти в существующий турнир
        </h3>
    </a>
</div>

<header class="masthead3" id="mast3">
    <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto text-center">
            <h2 class="text-white-50 mx-auto mt-2 mb-5">Организовать турнир</h2><br>
            <form action="/pcclub/createTournament" method="post">
                <div class="form-group row">
                    <div class="col-sm-10">
                        <input class="form-control" id="tournament_name" type="text" name="tournament_name"
                               placeholder="Название турнира" required><br>
                        <input class="form-control" type="date" name="dateFrom" placeholder="Дата начала турнира" required><br>
                        <input class="form-control" type="date" name="dateTo" placeholder="Дата конца турнира" required><br>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary js-scroll-trigger">Организовать</button>
            </form>
        </div>
    </div>
</header>

<header class="masthead4" id="mast4">
    <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto text-center">
            <h2 class="text-white-50 mx-auto mt-2 mb-5">Принять учатие</h2>
            <table class="table table-bordered table-hover table-dark">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">User_id</th>
                    <th scope="col">TimeFrom</th>
                    <th scope="col">TimeTo</th>
                    <th scope="col"></th>
                </tr>
                <c:forEach var="tournament" items="${tournaments}">
                    <tr>
                        <form action="joinTournament?tournament_id=${tournament.getId()}" method="post">
                            <th scope="row">${tournament.getId()}</th>
                            <td>${tournament.getName()}</td>
                            <td>${tournament.getUser_id()}</td>
                            <td>${tournament.getTimeFrom()}</td>
                            <td>${tournament.getTimeTo()}</td>
                            <td>
                                <button type="submit" class="btn-outline-dark">Вступить</button>
                            </td>
                        </form>
                    </tr>
                </c:forEach>
            </table>
            <a class="btn btn-primary js-scroll-trigger" href="#">Войти</a>
        </div>
    </div>
</header>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<!-- Core theme JS-->
<script src="resources/js/scripts.js"></script>
<!-- jquery JS -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Bootstrap js -->
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!-- Propeller textfield js -->
<script type="text/javascript" src="dist/js/propeller.min.js"></script>

<!-- Datepicker moment with locales -->
<script type="text/javascript" language="javascript" src="js/datePicker.js"></script>

</body>
</html>
